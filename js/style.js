"use strict";

const urlIP=`https://api.ipify.org/?format=json`;

const btn=document.querySelector(".search");

async function getIp (url){
    try{
        let promise=await fetch(url);
        return promise.json();
    } catch(err){
        console.log(err.message);
    }

}

async function getAdress({ip}){
    try{
        let promise=await fetch(`http://ip-api.com/json/${ip}?fields=status,continent,district,country,regionName,city`);
        return promise.json();
    } catch(err){
        console.log(err.message);
    }
}



btn.addEventListener('click',()=>{
    getIp(urlIP).then(data=>{
        getAdress(data).then(response=>{
            let p=document.createElement("p");
            for (const el in response) {
                console.log(response[el]);
                if(response[el]===""||response[el]===undefined||response[el]===null){
                    response[el]="no info";  
                }
            }
            p.textContent=`Континент:${response.continent
            }, Країна:${response.country}, Регіон:${response.regionName}, Місто:${response.city}, Район:${ response.district}`;
            document.body.append(p);
        });
    });
})